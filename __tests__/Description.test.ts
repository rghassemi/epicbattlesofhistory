import Description from "../game/Description";
import Fighter from "../game/Fighter";
import fighters from "./fixtures/Fighter";

test("should correctly make Description object", () => {
	const testDescription = () => "Test Description";
	const description = new Description(testDescription);
	expect(description).toBeInstanceOf(Description);
	expect(description.text).toBe(testDescription);
});

test("should return false when Description has no conditions", () => {
	const description = new Description(() => "Test Description");
	const result = description.checkConditions(null);
	expect(result).toBe(false);
});

test("should calculate conditions correctly", () => {
	const description = new Description(() => "Test Description", [
		() => true,
		() => true
	]);
	const result = description.checkConditions(null);
	expect(result).toBe(true);
});

test("should get text with fighter placeholders", () => {
	const description = new Description(
		(fighter: Fighter, opponent: Fighter) => `${fighter.name} ${opponent.name}`
	);
	const result = description.text(fighters[0], fighters[1]);
	expect(result).toBe(`${fighters[0].name} ${fighters[1].name}`);
});
