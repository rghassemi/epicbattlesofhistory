import Fighter, { FighterCategory } from "../game/Fighter";

test("fighter should be dead when one healthType is zero", () => {
	const fighter = new Fighter("Name 1", "Test description", FighterCategory.Person);
	expect(fighter.isDead()).toBe(false);

	fighter.health = 0;
	expect(fighter.isDead()).toBe(true);
});
