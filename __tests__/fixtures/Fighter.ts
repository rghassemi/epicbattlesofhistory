import Fighter from "../../game/Fighter";

const fighter1 = new Fighter("Name 1");
const fighter2 = new Fighter("Name 2");

export default [fighter1, fighter2];