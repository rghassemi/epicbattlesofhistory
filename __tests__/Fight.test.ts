import Fight from "../game/Fight";
import fighters from "./fixtures/Fighter";

describe("Initializing and Adding Fighters", () => {
	test("should create default Fight", () => {
		const fight = new Fight([]);
		expect(fight).toEqual({
			actionsTaken: 0,
			currentFighter: undefined,
			maxActions: 2,
			maxRounds: 3,
			order: [],
			round: 0,
			fighters: []
		});
	});

	test("should correctly add one fighter on constructor", () => {
		const fight = new Fight([fighters[0]]);
		expect(fight.fighters[0]).toEqual(fighters[0]);
	});

	test("should correctly add fighters on constructor", () => {
		const fight = new Fight(fighters);
		expect(fight.fighters[0]).toEqual(fighters[0]);
		expect(fight.fighters[1]).toEqual(fighters[1]);
	});

	test("should correctly add fighters on function call", () => {
		const fight = new Fight([fighters[0]]);
		fight.addFighter(fighters[1]);
		expect(fight.fighters[0]).toEqual(fighters[0]);
		expect(fight.fighters[1]).toEqual(fighters[1]);
	});
});

describe("Preparing fights", () => {
	test("should return expected text when starting fight", () => {
		const fight = new Fight([]);
		expect(fight.startFight()).toBe("Fight has started!");
	});

	test("should return expected text when starting round", () => {
		const fight = new Fight([]);
		expect(fight.startRound()).toBe("Starting next round!");
	});
});
