import ExecutionContext from "./alexa-skill/ExecutionContext";

let executionContext: ExecutionContext;

export async function handler(event: any, context: any) {
	console.log(`REQUEST++++${JSON.stringify(event)}`);

	if (!executionContext) {
		executionContext = new ExecutionContext(event);
	}

	return executionContext.skill.invoke(event, context);
}
