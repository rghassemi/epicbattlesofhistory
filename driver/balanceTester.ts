import Fight from "../game/Fight";
import Fighter from "../game/Fighter";
import { generateFighter, GenerateFighterResult } from "../game/generateFighter";
import { pickRandom, flatten } from "../util/generalUtility";
import Moves from "../game/Moves";
import { FighterPlaceholder } from "../game/DescriptionBuilder";
import Description from "../game/Description";
import Commentator from "../game/Commentator";

const MATCHUP_COUNT = 100;
const FIGHTS_PER_MATCHUP = 100;

const testFighters = [
	//Politicians
	"Barack Obama",
	"Donald Trump",
	"Hitler",
	//Singers
	"Miley Cyrus",
	"Britney Spears",
	//Authors
	"Patrick Rothfuss",
	"Brandon Sanderson",
	//Religious figures
	"Buddha",
	"Jesus Christ",
	//Actors
	"Arnold Schwarzenegger",
	"George Clooney",
	"Nathan Fillion",
	//Criminals
	"The Unibomber",
	"Jesse James",
	"Jack the Ripper",
	//Conquerors and military
	"Alexander the Great",
	"Genghis Khan",
	"Erwin Rommel",
	//Scientists
	"Albert Einstein",
	"Isaac Newton",
	"Steven Hawking",
	//Philosophers
	"Plato",
	"Aristotle",
	//Artists
	"Pablo Picasso",
	"Salvador Dali",
	"Vincent van Gogh",
	//Athletes
	"Babe Ruth",
	"Shaquille O'Neal",
	"Michael Jordan",
	"David Beckham"
];

async function main() {
	console.log(`Looking up all ${testFighters.length} fighters. May take a moment.`);
	const fighters = await makeFighters(testFighters);

	let timeouts = 0;
	const scoreboard = new Map<string, { wins: number; losses: number }>();
	const descriptionCounter = new Map<number, number>();
	fighters.forEach(fighter => {
		scoreboard.set(fighter.name, { wins: 0, losses: 0 });
	});

	console.log("Ready.");

	for (let i = 0; i < MATCHUP_COUNT; ++i) {
		simulate(FIGHTS_PER_MATCHUP);
		if (i % 10 === 0) console.log(`Completed ${i} of ${MATCHUP_COUNT} matchups.`);
	}

	output(console.log);

	function simulate(fightsPerMatch: number) {
		const fighterQueue = fighters.slice();
		const matchups = new Array<[Fighter, Fighter]>();
		while (fighterQueue.length > 1) {
			matchups.push([popRandom(fighterQueue), popRandom(fighterQueue)]);
		}

		for (let matchup of matchups) {
			//console.log(`Now simulating: ${matchup[0].name} vs. ${matchup[1].name}!`);
			for (let i = 0; i < fightsPerMatch; ++i) {
				const clones = matchup.map(fighter => fighter.clone());
				const fight = new Fight(clones);
				while (!fight.isGameOver()) {
					fight.nextState();
				}

				//Collect data
				const winner = clones.find(fighter => !fighter.isDead());
				const loser = clones.find(fighter => fighter.isDead());

				if (!winner || !loser) {
					++timeouts;
				} else {
					scoreboard.get(winner.name).wins++;
					scoreboard.get(loser.name).losses++;
				}

				const describers = fight.fighters.concat([<any>fight.commentator]);
				describers
					.map(describer => describer.usedDescriptions)
					.reduce((a, b) => a.concat(b))
					.forEach(id => {
						if (descriptionCounter.has(id)) {
							descriptionCounter.set(id, descriptionCounter.get(id) + 1);
						} else {
							descriptionCounter.set(id, 1);
						}
					});
			}
		}
	}

	async function makeFighters(names: Array<string>) {
		//Lookup fighters 10 at a time, to avoid overloading wikipedia with too many requests
		let genFighterResults: Array<GenerateFighterResult> = [];
		for (let start = 0; start < names.length; start += 10) {
			const end = Math.min(start + 10, names.length);
			const batch = await Promise.all(
				names.slice(start, end).map(name => generateFighter(name))
			);
			genFighterResults = genFighterResults.concat(batch);
			if (end === names.length) break;
		}

		const invalid = genFighterResults.filter(result => !result.valid);
		if (invalid.length > 0) {
			invalid.forEach(badFighter => {
				console.log(
					`Invalid fighter in testFighters: ${badFighter.name}, Reason: ${
						badFighter.reason
					}`
				);
			});
			return null;
		}
		return genFighterResults.map(result => result.fighter);
	}

	//Randomly pair off the fighers
	function popRandom<T>(options: Array<T>) {
		const random = pickRandom(options);
		options.splice(options.indexOf(random), 1);
		return random;
	}

	function output(write: (log: string) => void) {
		write("Name, Winrate");
		scoreboard.forEach((score, name) => {
			write(`${name},${score.wins / (score.wins + score.losses)}`);
		});
		write(`Timeouts: ${timeouts}`);

		//Description list
		const extractDesc = (
			descriptions: Array<Description>,
			move: string,
			type = "unspecified"
		) => {
			return descriptions.map(desc => {
				return {
					id: desc.id,
					data: {
						type: type,
						text: `${move},${desc.text(
							new FighterPlaceholder("Bob"),
							new FighterPlaceholder("Alice")
						)}`
					}
				};
			});
		};
		const moveDescriptions = new Map<number, any>();
		Moves.map(move =>
			extractDesc(move.hit, move.name, "hit")
				.concat(extractDesc(move.miss, move.name, "miss"))
				.concat(extractDesc(move.win, move.name, "win"))
		)
			.reduce((m1, m2) => m1.concat(m2))
			.forEach(flatDesc => moveDescriptions.set(flatDesc.id, flatDesc.data));

		//Output move data
		write("ID, Count, Move, Type, Description");
		descriptionCounter.forEach((count, id) => {
			if (moveDescriptions.has(id)) {
				const record = moveDescriptions.get(id);
				write(`${id}, ${count}, ${record.type}, ${record.text}`);
			}
		});

		const name = "Comentator";
		const commentary = new Map<number, any>();
		extractDesc(Commentator.startFightCommenetary, name)
			.concat(extractDesc(Commentator.cheerCommentary, name))
			.concat(extractDesc(Commentator.booCommentary, name))
			.concat(
				extractDesc(
					flatten(Commentator.commentary.map(inner => flatten(inner))),
					name
				)
			)
			.forEach(comment => commentary.set(comment.id, comment.data));
		write("ID, Count, Move, Type, Description");
		descriptionCounter.forEach((count, id) => {
			if (commentary.has(id)) {
				const record = commentary.get(id);
				write(`${id}, ${count}, ${record.type}, ${record.text}`);
			}
		});
	}
}

main();
