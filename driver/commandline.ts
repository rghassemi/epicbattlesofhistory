import { generateFighter } from "../game/generateFighter";
import Fighter from "../game/Fighter";
import Fight from "../game/Fight";
import { FightState } from "../game/Fight";
import * as inquirer from "inquirer";
import * as colors from "colors";

let skipInput = false;
const fighters: Array<Fighter> = [];

type AddFighterChoice = { name: string };
type BettingChoice = { fighter1: number; fighter2: number };
type CheerOrBoo = { answer: "Cheer" | "Boo" };

const prompt = async (...fighterNames: Array<string>) => {
	const printAction = (text: string) => {
		console.log(colors.yellow(text));
	};

	const makeFighter = async (name: string) => {
		const result = await generateFighter(name);
		if (result.valid) fighters.push(result.fighter);
		else console.log(result.reason);
	};

	for (const name of fighterNames) {
		await makeFighter(name);
	}

	while (fighters.length < 2) {
		const answers = await inquirer.prompt<AddFighterChoice>({
			type: "input",
			name: "name",
			message: "Add a fighter"
		});
		if (answers.name) {
			await makeFighter(answers.name);
		}
	}

	const fight = new Fight(fighters);

	while (fight.state !== FightState.End) {
		switch (fight.state) {
			case FightState.Betting:
				if (!skipInput) {
					const answers = await inquirer.prompt<BettingChoice>([
						{
							type: "input",
							name: "fighter1",
							message: `Any bets on ${fight.fighters[0].name}?`
						},
						{
							type: "input",
							name: "fighter2",
							message: `Any bets on ${fight.fighters[1].name}?`
						}
					]);
					printAction(fight.addBet(fight.fighters[0], answers.fighter1));
					printAction(fight.addBet(fight.fighters[1], answers.fighter2));
				}
				break;
			case FightState.InRound:
				break;
			case FightState.BetweenRound:
				break;
			case FightState.Interaction:
				if (!skipInput) {
					const result = await inquirer.prompt<CheerOrBoo>([
						{
							type: "list",
							name: "answer",
							message: "",
							choices: ["Cheer", "Boo"]
						}
					]);
					printAction(fight.cheerOrBoo(result.answer === "Cheer"));
				}
				break;
		}
		const output = fight.nextState();
		printAction(output.getSpeech());
	}

	//Fight over
	const output = fight.nextState();
	console.log(colors.yellow(output.getSpeech()));
};
if (process.argv.length === 5) {
	const fighter1 = process.argv[2];
	const fighter2 = process.argv[3];
	skipInput = true;
	prompt(fighter1, fighter2);
} else if (process.argv.length === 4) {
	const fighter1 = process.argv[2];
	const fighter2 = process.argv[3];
	prompt(fighter1, fighter2);
} else if (process.argv.length === 3) {
	skipInput = true;
	prompt();
} else {
	prompt();
}
