import {
	copySync,
	existsSync,
	ensureDirSync,
	readFileSync,
	removeSync,
	chmodSync,
	createWriteStream
} from "fs-extra";
import { Lambda, config } from "aws-sdk";
import * as archiver from "archiver";
import * as colors from "colors";

const rootDir = "."; //TODO: Specify in less brittle way
const functionArn =
	"arn:aws:lambda:us-west-2:059391986172:function:EpicBattlesOfHistorySkill";

const nodeModules = `${rootDir}/node_modules`;
const codePath = `${rootDir}/js`;
const packagePath = `${rootDir}/package.json`;
const outputFolder = `${rootDir}/lambdaBuild`;
const outputArchive = `${rootDir}/lambdaBuild.zip`;

export async function lambdaDeploy() {
	if (process.argv.length < 2) {
		console.log("Warning: No prod package list was provided. Will bundle ALL deps.");
	}
	const prodPackagesList = parseProdPackageList(process.argv[2]);
	console.log("Collecting all code and node_modules");
	const zip = await packageLambda(prodPackagesList);
	console.log("Packaging finished. Uploading...");
	await deployPackage(zip);
	console.log('Testing lambda function: "Launch Wiki Battles"');
	await testLambda(getTestCall());
	cleanup();
}

function parseProdPackageList(packageListPath: string) {
	const packageListStr = readFileSync(packageListPath, { encoding: "utf8" }); //{ encoding: "utf16le" });
	const packageList = packageListStr.split("\n");
	if (packageList.length < 2) return new Array<string>();
	const baseModulesPath = packageList.shift().trim();
	const packages = packageList
		.map(packagePath =>
			packagePath.trim().replace(baseModulesPath + "\\node_modules\\", "")
		)
		.filter(packagePath => packagePath !== "" && !packagePath.includes("@types"));
	return packages;
}

async function packageLambda(allowedModules: Array<string>) {
	if (!existsSync(nodeModules) || !existsSync(`${codePath}/index.js`)) {
		console.log("Make sure index.js exists, along with node_modules folder.");
		return;
	}

	cleanup();

	ensureDirSync(outputFolder);

	const devDependencies = copySync(nodeModules, `${outputFolder}/node_modules`, {
		overwrite: true,
		filter: (src: string, dest: string) => {
			if (allowedModules.length === 0) return true;
			else if (src.endsWith("/node_modules")) return true;
			else return allowedModules.some(moduleName => src.includes(moduleName));
		}
	});

	copySync(codePath, outputFolder, {
		overwrite: true,
		filter: (src: string, dest: string) => {
			//Only Js code, and exclude tools and tests
			return !(
				src.endsWith(".map") ||
				src.endsWith("lambdaDeploy.js") ||
				src.endsWith("__tests__")
			);
		}
	});

	copySync(packagePath, outputFolder + "/package.json", { overwrite: true });

	console.log("Zipping code");
	await zipFolderAsync(outputFolder, outputArchive);
	return outputArchive;
}

function zipFolderAsync(folder: string, outputArchive: string) {
	return new Promise((resolve, reject) => {
		var output = createWriteStream(outputArchive);
		var zipArchive = archiver("zip", { zlib: { level: 9 } });

		output.on("close", function() {
			resolve();
		});
		zipArchive.pipe(output);
		zipArchive.glob(`**/*`, { dot: true, cwd: folder }, {});
		zipArchive.finalize();
	});
}

function deployPackage(zip: string) {
	return new Promise((resolve, reject) => {
		config.update({ region: "us-west-2" });
		var lambda = new Lambda();

		lambda.updateFunctionCode(
			{ ZipFile: readFileSync(zip), FunctionName: functionArn },
			(err, data) => {
				if (err) {
					console.log(err, err.stack);
					reject(err);
				} else {
					console.log(
						`Successfully updated ${data.FunctionName}. Code size: ${
							data.CodeSize
						}`
					);
					resolve(true);
				}
			}
		);
	});
}

function testLambda(testInput: string) {
	var lambda = new Lambda();
	return new Promise((resolve, reject) => {
		lambda.invoke(
			{
				FunctionName: functionArn,
				InvocationType: "RequestResponse",
				//LogType: "Tail",
				Payload: testInput
			},
			function(err, data) {
				if (err) {
					console.log("Test failed.");
					console.log(err, err.stack);
					reject();
				} else {
					console.log("Deploy success.");
					const payload = JSON.parse(data.Payload.toString());
					if (payload.errorMessage) {
						console.log(
							colors.bold(colors.red(`ERROR: ${payload.errorType}`))
						);
						console.log(colors.bold(colors.red(`${payload.errorMessage}`)));
						console.log(colors.red(`STACK: ${payload.stackTrace}`));
					} else {
						if (payload.response && payload.response.outputSpeech) {
							console.log(
								colors.bold(colors.green(payload.response.outputSpeech))
							);
						}
						console.log(data);
					}
					resolve();
				}
			}
		);
	});
}

function cleanup() {
	console.log("Removing temp folders and archives.");
	removeSync(outputArchive);
	removeSync(outputFolder);
	removeSync("prodPackageFull.txt");
}

lambdaDeploy();

//Use function hoisting to hide this mess down here
function getTestCall() {
	return JSON.stringify({
		version: "1.0",
		session: {
			new: true,
			sessionId: "amzn1.echo-api.session.42cc1f1f-fee4-4d70-b142-799fe7f0b41b",
			application: {
				applicationId: "amzn1.ask.skill.e1ba1ab1-6b4c-462f-a7d2-dc9b8650976c"
			},
			user: {
				userId:
					"amzn1.ask.account.AFLULBRUJAMPCOH646RR4FR5YHG3UAQTI2UUO4C2LTCGUN7EUNWTA24KNLZBCWJA4TE5LI7H7ENE5IWCZIAEANW7OXUNRB6VNG6JWAPNY3H7IPYEC2REXMHQLYERCITWAVHJRLYPPKMVTHBPI6BKFKTKGNQ5MC2CC4MFQTOCL5BJEBDJ6DY4TANRQIQBOHZCHT6YGUPNBCRVKGI"
			}
		},
		context: {
			AudioPlayer: {
				playerActivity: "IDLE"
			},
			System: {
				application: {
					applicationId: "amzn1.ask.skill.e1ba1ab1-6b4c-462f-a7d2-dc9b8650976c"
				},
				user: {
					userId:
						"amzn1.ask.account.AFLULBRUJAMPCOH646RR4FR5YHG3UAQTI2UUO4C2LTCGUN7EUNWTA24KNLZBCWJA4TE5LI7H7ENE5IWCZIAEANW7OXUNRB6VNG6JWAPNY3H7IPYEC2REXMHQLYERCITWAVHJRLYPPKMVTHBPI6BKFKTKGNQ5MC2CC4MFQTOCL5BJEBDJ6DY4TANRQIQBOHZCHT6YGUPNBCRVKGI"
				},
				device: {
					deviceId:
						"amzn1.ask.device.AFESWMGRSJO6NQKJ7FWZEIQH6VCGN4N4X6HGXXTDFAND4GPM4KEJQANWBZC4ODGHA5LP5SGQUDSREL52GMWV2Q7EPLYC625O5YH2M2U5DVOBSXXLJ6FCSUXWE5BKMV5RAF3UI35GKTAB4DUCZ3OYTEE65K6LOTWXLMGR4AO34KCPVEVTJYEAO",
					supportedInterfaces: {
						AudioPlayer: {}
					}
				},
				apiEndpoint: "https://api.amazonalexa.com",
				apiAccessToken:
					"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjEifQ.eyJhdWQiOiJodHRwczovL2FwaS5hbWF6b25hbGV4YS5jb20iLCJpc3MiOiJBbGV4YVNraWxsS2l0Iiwic3ViIjoiYW16bjEuYXNrLnNraWxsLmUxYmExYWIxLTZiNGMtNDYyZi1hN2QyLWRjOWI4NjUwOTc2YyIsImV4cCI6MTUzMjA4MDU5MiwiaWF0IjoxNTMyMDc2OTkyLCJuYmYiOjE1MzIwNzY5OTIsInByaXZhdGVDbGFpbXMiOnsiY29uc2VudFRva2VuIjpudWxsLCJkZXZpY2VJZCI6ImFtem4xLmFzay5kZXZpY2UuQUZFU1dNR1JTSk82TlFLSjdGV1pFSVFINlZDR040TjRYNkhHWFhUREZBTkQ0R1BNNEtFSlFBTldCWkM0T0RHSEE1TFA1U0dRVURTUkVMNTJHTVdWMlE3RVBMWUM2MjVPNVlIMk0yVTVEVk9CU1hYTEo2RkNTVVhXRTVCS01WNVJBRjNVSTM1R0tUQUI0RFVDWjNPWVRFRTY1SzZMT1RXWExNR1I0QU8zNEtDUFZFVlRKWUVBTyIsInVzZXJJZCI6ImFtem4xLmFzay5hY2NvdW50LkFGTFVMQlJVSkFNUENPSDY0NlJSNEZSNVlIRzNVQVFUSTJVVU80QzJMVENHVU43RVVOV1RBMjRLTkxaQkNXSkE0VEU1TEk3SDdFTkU1SVdDWklBRUFOVzdPWFVOUkI2Vk5HNkpXQVBOWTNIN0lQWUVDMlJFWE1IUUxZRVJDSVRXQVZISlJMWVBQS01WVEhCUEk2QktGS1RLR05RNU1DMkNDNE1GUVRPQ0w1QkpFQkRKNkRZNFRBTlJRSVFCT0haQ0hUNllHVVBOQkNSVktHSSJ9fQ.RFlwzuQ951aMFiJYn_WwoQtV8yIH881KzpFn8QArWcvWmJetmCD3KGo7yEEILQH-VG9wYBSwqSx_e7uuEkq6GIPkg9YQ4P7HOIpOMPXY1JxWPofYznY7xNJUIs0G9GH-s1oCdd_unR8D5UykrEBkukqrsNuedmNEg_NuQ2bLp3r4nXLIIGgQYKM6bvfx127z7wVfollLkOGcN13cnS9VpgFUDXYv6dWH9oWzL5YR3QF0Jgg3d82gl3e2qbu-3a8Gf-n-u1678s9ZEl7qWwYpfv7j-KkgSpWbYUADoC4wz0tyKRe9pdqM0EcYFd1tLUaOKCNUi1J6dUdoKnFtgMy_pw"
			}
		},
		request: {
			type: "LaunchRequest",
			requestId: "amzn1.echo-api.request.88838956-6597-457c-8eaf-e7469ebd96d0",
			timestamp: "2018-07-20T08:56:32Z",
			locale: "en-US",
			shouldLinkResultBeReturned: false
		}
	});
}
