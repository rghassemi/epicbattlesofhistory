import Axios from "axios";
import { safeGet } from "./generalUtility";

type WikiSearchResult = {
	label: string;
	uri: string;
	id: string;
};

type WikiEntity = {
	label: string;
	aliases: Array<string>;
	claims: any;
};

export async function wikiSearch(name: string) {
	let words = name.split(" ");
	let searchResults: Array<any> = null;
	while (words.length > 0 && (!searchResults || searchResults.length === 0)) {
		const term = words.reduce((word1, word2) => word1 + "%20" + word2);
		const wikisearch = "https://www.wikidata.org/w/api.php?action=wbsearchentities";
		const query = "&search=" + term + "&format=json&language=en&type=item&continue=0";
		const response = await Axios.get(wikisearch + query);
		searchResults = response.data.search;
		words.shift();
	}
	if (!searchResults || searchResults.length === 0) return null;

	const firstResult = searchResults[0];
	return {
		label: firstResult.label,
		uri: firstResult.concepturi,
		id: firstResult.id
	};
}

export async function getWikiEntity(id: string): Promise<WikiEntity> {
	const wikiEntity = "https://www.wikidata.org/wiki/Special:EntityData/";
	const detailsResponse = await Axios.get(wikiEntity + id + ".json");
	const details = safeGet(detailsResponse.data, "entities", id);
	const label = safeGet(details, "labels", "en", "value") || "Unknown Name";
	const aliases = safeGet(details, "aliases", "en") || [];
	return {
		label: label,
		aliases: aliases.map((alias: any) => alias.value),
		claims: details.claims
	};
}

type Property = "Occupation" | "Nationality";
const PropertyIds = {
	Occupation: "P106",
	Nationality: "P27"
};

export async function getProperty(entity: WikiEntity, propertyId: Property) {
	const claims = entity.claims;
	const id =
		safeGet(
			claims,
			PropertyIds[propertyId],
			0,
			"mainsnak",
			"datavalue",
			"value",
			"id"
		) || null;
	if (id) {
		return await getWikiEntity(id);
	}
	return null;
}
