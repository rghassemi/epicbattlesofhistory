export interface MapObject<T> {
	[key: string]: T;
}

export function safeGet(object: any, ...path: Array<string | number>) {
	let current = object;
	for (let i = 0; i < path.length; ++i) {
		const key = path[i];
		if (current[key] == undefined) {
			return null;
		}
		if (i === path.length - 1) {
			return current[key];
		}
		current = current[key];
	}
	return null;
}

export function pickRandom<T>(options: Array<T>) {
	const index = Math.floor(Math.random() * options.length);
	return options[index];
}

export function flatten<T>(layered: Array<Array<T>>) {
	return layered.reduce((a, b) => a.concat(b));
}
