import Axios from "axios";

export const getFigureId = async (name: string) => {
	let words = name.split(" ");
	let searchResults: Array<any> = null;
	while (words.length > 0 && (!searchResults || searchResults.length === 0)) {
		const term = words.reduce((word1, word2) => word1 + "+" + word2);
		const wikisearch = "https://www.wikidata.org/w/api.php?action=wbsearchentities";
		const query = "&search=" + term + "&format=json&language=en&type=item&continue=0";
		// console.log(`Looking up ${name} at ${wikisearch + query}`);
		const response = await Axios.get(wikisearch + query);
		searchResults = response.data.search;
		words.shift();
	}
	if (!searchResults || searchResults.length === 0) return null;

	const firstResult = searchResults[0];
	// console.log(JSON.stringify(firstResult));
	return {
		id: firstResult.id as string,
		label: firstResult.label as string,
		description: firstResult.description as string
	};
};

export const generateSparQLQuery = (figureId: string) =>
	`SELECT ?occupationLabel ?nationalityLabel ?genderLabel ?dobLabel ?deadLabel ?superclassLabel ?workLabel WHERE {
		{ wd:${figureId} wdt:P31 ?superclass. }
		UNION
		{ wd:${figureId} wdt:P27 ?nationality. }
		UNION
		{ wd:${figureId} wdt:P106 ?occupation. }
		UNION
		{ wd:${figureId} wdt:P21 ?gender. }
		UNION
		{ wd:${figureId} wdt:P569 ?dob. }
		UNION
		{ wd:${figureId} wdt:P570 ?dead. }
		UNION
		{  wd:${figureId} wdt:P800 ?work. }
		SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
	}`;

export const getSparqlQueryResults = async (figureId: string) => {
	const endpointUrl = "https://query.wikidata.org/sparql";
	const query = generateSparQLQuery(figureId);
	const fullUrl = endpointUrl + "?query=" + encodeURIComponent(query);
	const headers = { Accept: "application/sparql-results+json" };
	const body = await Axios.get(fullUrl, { headers });
	const {
		head: { vars },
		results
	} = body.data;
	return results.bindings;
};
