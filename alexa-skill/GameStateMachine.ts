import Fight, { FightState } from "../game/Fight";
import { Slots, getSlot } from "./AlexaIntentHandler";
import { generateFighter } from "../game/generateFighter";
import Fighter from "../game/Fighter";
import Speech from "../game/Speech";

type Phase = "Matchmaking" | "PreFight" | "RoundBreak" | "PostFight";

export type GameIntentHandler = (intent: string, slots: Slots) => Promise<Speech>;

export default class GameStateMachine {
	fight: Fight;

	constructor() {
		this.fight = null;
	}

	static fromJson(object: any): GameStateMachine {
		const fight = Fight.fromJson(object.fight);
		Object.setPrototypeOf(object, GameStateMachine);
		return object as GameStateMachine;
	}

	help() {
		if (!this.fight) {
			return Fight.instructions(FightState.Matchmaking);
		}
		const instructions = Fight.instructions(this.fight);
		//Should handle in-progress interputions?
		return instructions;
	}

	routeGameIntents(intentName: string): GameIntentHandler | null {
		const state = this.fight ? this.fight.state : FightState.Matchmaking;
		switch (state) {
			case FightState.Matchmaking:
				switch (intentName) {
					case "ChooseFighter":
						return this.OnMatchmaking;
				}
				break;
			case FightState.Betting:
				switch (intentName) {
					case "PlaceBet":
					case "NoBet":
					case "AMAZON.NoIntent":
						return this.onPreFight;
				}
			case FightState.InRound:
			case FightState.BetweenRound:
			//nothing here..., not expecting interactions
			case FightState.Interaction:
				const interactFighter = this.fight.fighters[
					this.fight.interactingFighterIndex
				];
				switch (intentName) {
					case "Cheer":
					case "Boo":
					case "AMAZON.NoIntent":
						return this.onInteraction;
				}
		}
		return null;
	}

	async maybeCreateFighters(slots: Slots) {
		const tryMakeFighter = async (slotName: string) => {
			const name = getSlot(slots, slotName);
			if (name === null) return null;
			return await generateFighter(name);
		};

		return [
			await tryMakeFighter("fighterName"),
			await tryMakeFighter("fighterNameA"),
			await tryMakeFighter("fighterNameB")
		];
	}

	private OnMatchmaking = async (intent: string, slots: Slots) => {
		let maybeFighters = await this.maybeCreateFighters(slots);

		if (!this.fight) {
			this.fight = new Fight([]);
		}

		let created: typeof maybeFighters;
		if (maybeFighters[0]) {
			created = [maybeFighters[0]];
		} else {
			created = [];
			if (maybeFighters[1]) created.push(maybeFighters[1]);
			if (maybeFighters[2]) created.push(maybeFighters[2]);
		}

		const response = Speech.create();
		for (const thing of created) {
			if (thing.valid) {
				response.concat(this.fight.addFighter(thing.fighter));
			} else {
				const maybeName = thing.fighter ? thing.fighter.name : "";
				response.speak(`Sorry, ${maybeName} doesn't work.`);
				response.speak(thing.reason);
			}
		}

		response.concat(this.fight.nextState());

		return response;
	};

	private onPreFight = async (intent: string, slots: Slots) => {
		const response = Speech.create();
		if (!this.processBet(intent, slots, response)) {
			return response;
		}
		response.pause();
		return this.progressFight(response);
	};

	private onInteraction = async (intent: string, slots: Slots) => {
		const fight = this.fight;
		const response = Speech.create();

		if (intent === "Cheer" || intent === "Boo") {
			response.speak(fight.cheerOrBoo(intent === "Cheer"));
			response.endSentence();
			response.concat(fight.nextState());
		}

		return this.progressFight(response);
	};

	private progressFight(response: Speech) {
		const fight = this.fight;

		while (
			!(fight.state === FightState.End || fight.state === FightState.Interaction)
		) {
			response.concat(fight.nextState());
		}
		if (fight.state === FightState.End) {
			response.concat(fight.nextState());
		}

		return response;
	}

	private processBet(intent: string, slots: Slots, response: Speech) {
		const fight = this.fight;
		if (intent === "NoIntent" || intent === "NoBet") {
			response.speak("No bets.");
			response.endSentence();
			return true;
		}
		let betFighterName = getSlot(slots, "betFighter");
		const betAmount = parseInt(getSlot(slots, "betAmount")) || false;

		if (betAmount === 0) {
			response.speak("Zero bets.");
			response.endSentence();
			return true;
		}

		let badBetName = false;
		if (!betFighterName) {
			badBetName = true;
		} else {
			betFighterName = betFighterName.toLowerCase();
			var betFighter = fight.fighters.find(
				fighter =>
					fighter.name === betFighterName ||
					fighter.name.toLowerCase().includes(betFighterName)
			);
			if (!betFighter) {
				badBetName = true;
			}
		}

		if (betAmount === false || badBetName) {
			const exampleName = this.fight.fighters[0].name;
			response.speak(
				`Didn't get that. Try saying, I bet 10 on ${exampleName}, or, no bets.`
			);
			return false;
		}

		response.speak(fight.addBet(betFighter, betAmount));
		response.endSentence();

		return true;
	}
}
