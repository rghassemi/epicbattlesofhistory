import { IntentRequest, Slot, Response, Session, Intent } from "ask-sdk-model";
import * as Alexa from "ask-sdk-core";
import { safeGet } from "../util/generalUtility";

export interface Slots {
	[key: string]: Slot;
}

export type IntentHandler = (
	intent: Intent,
	response: Alexa.ResponseBuilder,
	input?: Alexa.HandlerInput
) => void;

export function getSlot(slots: Slots, name: string): string {
	if (slots && slots[name] && slots[name].value) {
		return slots[name].value;
	}
	return null;
}

export class AlexaIntentHandler {
	intentNames = new Set<string>();

	private acceptAllIntents = false;

	constructor(
		public type: string,
		intents: Array<string> | string,
		public handler: IntentHandler
	) {
		if (typeof intents === "string") {
			if (intents === "*") {
				this.acceptAllIntents = true;
			} else this.intentNames.add(intents);
		} else {
			intents.forEach(name => this.intentNames.add(name));
		}
	}

	canHandle(input: Alexa.HandlerInput) {
		const request = input.requestEnvelope.request as IntentRequest;
		const type = request.type;
		const name = safeGet(request, "intent", "name");
		return (
			this.type === type && (this.acceptAllIntents || this.intentNames.has(name))
		);
	}

	async handle(input: Alexa.HandlerInput) {
		const request = input.requestEnvelope.request as IntentRequest;
		const intent = request.intent;

		let responseBuilder = input.responseBuilder;
		if (
			(request.type as any) !== "CanFulfillIntentRequest" &&
			request.dialogState &&
			request.dialogState !== "COMPLETED"
		) {
			return responseBuilder
				.addDelegateDirective()
				.withShouldEndSession(false)
				.getResponse();
		}
		const name = safeGet(request, "intent", "name");
		await this.handler(intent, responseBuilder, input);
		return responseBuilder.getResponse();
	}
}
