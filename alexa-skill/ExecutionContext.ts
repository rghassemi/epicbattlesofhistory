import * as Alexa from "ask-sdk-core";
import { AlexaIntentHandler } from "./AlexaIntentHandler";
import GameIntentHandler from "./GameIntentHandler";
import { RequestEnvelope } from "ask-sdk-model";

export default class ExecutionContext {
	skill: Alexa.Skill;
	gameHandler: GameIntentHandler;
	canFufillIntentHandler: AlexaIntentHandler;
	launchHandler: AlexaIntentHandler;
	cancelHandler: AlexaIntentHandler;
	sessionEndHandler: AlexaIntentHandler;
	fallbackHandler: AlexaIntentHandler;
	helpHandler: AlexaIntentHandler;
	repeatHandler: AlexaIntentHandler;
	restartHandler: AlexaIntentHandler;

	constructor(request: RequestEnvelope) {
		this.gameHandler = new GameIntentHandler(request);

		this.canFufillIntentHandler = new AlexaIntentHandler(
			"CanFulfillIntentRequest",
			"*",
			async (intent, response, input) => {
				console.log("Hit canFulfillIntent endpoint!!");
				response = await this.gameHandler.recognizeFighters(intent, response);
				return response.getResponse();
			}
		);

		this.launchHandler = new AlexaIntentHandler(
			"LaunchRequest",
			"*",
			(intent, response, input) => {
				const speechText = "Welcome to Wiki Battles. Choose a fighter.";

				if (this.gameHandler.state) {
					this.gameHandler.reset();
				}

				return response
					.speak(this.gameHandler.welcome())
					.reprompt(this.gameHandler.help());
			}
		);

		this.cancelHandler = new AlexaIntentHandler(
			"IntentRequest",
			["AMAZON.CancelIntent", "AMAZON.StopIntent"],
			(intent, response, input) => {
				if (this.gameHandler && this.gameHandler.state) {
					this.gameHandler.reset();
				}

				const speech = "Goodbye!";
				return response
					.speak(speech)
					.withShouldEndSession(true)
					.getResponse();
			}
		);

		this.sessionEndHandler = new AlexaIntentHandler(
			"SessionEndedRequest",
			"*",
			(intent, response, input) => {
				//any cleanup logic goes here
				if (this.gameHandler && this.gameHandler.state) {
					this.gameHandler.reset();
				}
				return response.speak("Ending the fight.").getResponse();
			}
		);

		this.fallbackHandler = new AlexaIntentHandler(
			"IntentRequest",
			"AMAZON.FallbackIntent",
			(intent, response) => {
				return response
					.speak("Sorry, I did not quite get that.")
					.speak(this.gameHandler.help())
					.reprompt(this.gameHandler.help())
					.withShouldEndSession(false)
					.getResponse();
			}
		);

		this.helpHandler = new AlexaIntentHandler(
			"IntentRequest",
			"AMAZON.HelpIntent",
			(intent, response) => {
				return response
					.speak(this.gameHandler.help())
					.reprompt(this.gameHandler.help())
					.withShouldEndSession(false)
					.getResponse();
			}
		);

		this.repeatHandler = new AlexaIntentHandler(
			"IntentRequest",
			"AMAZON.RepeatIntent",
			(intent, response) => {
				const { text, reprompt } = this.gameHandler.lastSpeech;
				return response
					.speak(text)
					.reprompt(reprompt)
					.withShouldEndSession(false)
					.getResponse();
			}
		);

		this.restartHandler = new AlexaIntentHandler(
			"IntentRequest",
			"AMAZON.StartOverIntent",
			(intent, response, input) => {
				return this.launchHandler.handler(intent, response);
			}
		);

		this.skill = Alexa.SkillBuilders.custom()
			.addRequestHandlers(
				this.canFufillIntentHandler,
				this.launchHandler,
				this.cancelHandler,
				this.restartHandler,
				this.repeatHandler,
				this.helpHandler,
				this.fallbackHandler,
				this.gameHandler,
				this.sessionEndHandler
			)
			.create();
	}
}
