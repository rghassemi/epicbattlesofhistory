import * as Alexa from "ask-sdk-core";
import { Intent } from "ask-sdk-model";
import { AlexaIntentHandler, IntentHandler } from "./AlexaIntentHandler";
import GameStateMachine from "./GameStateMachine";
import AlexaSpeech from "./AlexaSpeech";
import Speech from "../game/Speech";
import { FightState } from "../game/Fight";
import { safeGet } from "../util/generalUtility";
import { RequestEnvelope } from "ask-sdk-model";

export default class GameIntentHandler extends AlexaIntentHandler {
	state: GameStateMachine;
	lastSpeech: { text: string; reprompt: string };

	constructor(request: RequestEnvelope) {
		super("IntentRequest", "*", null);
		super.handler = this.handleGameIntents;

		//Configure all speech to use Alexa stuff.
		Speech.configure(() => new AlexaSpeech());

		this.restoreGameState(request);
		console.log("State set to: " + JSON.stringify(this.state));

		this.lastSpeech = { text: this.welcome(), reprompt: this.welcome() };
	}

	reset() {
		this.state.fight = null;
		this.lastSpeech = { text: this.welcome(), reprompt: this.welcome() };
	}

	welcome() {
		return "Welcome to Wiki Battles. Choose a fighter.";
	}

	help() {
		return this.state.help();
	}

	//TODO: This is way to complicated. Probably simplify intent.
	async recognizeFighters(intent: Intent, responseBuilder: Alexa.ResponseBuilder) {
		const name = intent.name;
		if (name === "ChooseFighter") {
			console.log("ChooseFighters intent canFufill");
			const slots = intent.slots;
			const maybeFighters = await this.state.maybeCreateFighters(slots);
			if (!maybeFighters.every(entry => entry == null)) {
				const validQuery = maybeFighters[0].valid;
				const validNameA = maybeFighters[1].valid;
				const validNameB = maybeFighters[2].valid;
				const validAB = validNameA || validNameB;
				return responseBuilder.withCanFulfillIntent({
					canFulfill: "YES",
					slots: {
						fighterName: {
							canUnderstand: validAB || validQuery ? "YES" : "MAYBE",
							canFulfill: "YES"
						},
						fighterNameA: {
							canUnderstand: validQuery || validNameA ? "YES" : "MAYBE",
							canFulfill: "YES"
						},
						fighterNameB: {
							canUnderstand: validQuery || validNameB ? "YES" : "MAYBE",
							canFulfill: "YES"
						}
					}
				});
			}
		}

		console.log("Failing canfufill");
		return responseBuilder.withCanFulfillIntent({ canFulfill: "NO" });
	}

	private handleGameIntents: IntentHandler = async (intent, responseBuilder, input) => {
		const name = intent.name;
		const slots = intent.slots;

		console.log("State is: " + JSON.stringify(this.state));

		const intentHandler = this.state.routeGameIntents(name);
		if (intentHandler === null) {
			console.log(`EPOH: Unrecognized Intent: ${name}, ${JSON.stringify(input)}.`);
			return responseBuilder
				.speak(this.help())
				.reprompt(this.help())
				.withShouldEndSession(false)
				.getResponse();
		}

		const speech = await intentHandler(name, slots);
		this.lastSpeech.text = speech.getSpeech();
		this.lastSpeech.reprompt = this.help();
		console.log(this.lastSpeech.text);
		responseBuilder.speak(this.lastSpeech.text).reprompt(this.lastSpeech.reprompt);

		if (this.state.fight.state === FightState.End) {
			console.log(`Ending session with PostFight received`);
			return responseBuilder.withShouldEndSession(true).getResponse();
		}

		this.saveGameState(input.attributesManager);

		return responseBuilder.withShouldEndSession(false).getResponse();
	};

	private saveGameState(attributesManager: Alexa.AttributesManager) {
		console.log("Saving state");
		attributesManager.setSessionAttributes({
			state: JSON.stringify(this.state),
			lastSpeech: this.lastSpeech
		});
	}

	private restoreGameState(request: RequestEnvelope) {
		//Deserialize GameState from session if needed
		console.log(JSON.stringify(request));
		const sessionState = safeGet(request, "session", "attributes", "state");
		console.log("session state is: " + sessionState);
		if (sessionState) {
			console.log(`Found state in session`);
			this.state = GameStateMachine.fromJson(JSON.parse(sessionState));
			this.lastSpeech = safeGet(request, "session", "attributes", "lastSpeech");
		} else {
			console.log("No session data found");
			this.state = new GameStateMachine();
		}
	}
}
