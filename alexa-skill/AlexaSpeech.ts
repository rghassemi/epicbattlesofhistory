import Speech from "../game/Speech";

export default class AlexaSpeech extends Speech {
	constructor() {
		super();
	}

	endSentence() {
		this.speech.push('<break strength="strong"/>');
		return this;
	}

	pause() {
		this.speech.push('<break strength="x-strong"/>');
		return this;
	}
}
