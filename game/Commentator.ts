import { pickRandom } from "../util/generalUtility";
import DescriptionBuilder from "./DescriptionBuilder";
import Fighter from "./Fighter";
import Description from "./Description";
import Fight from "./Fight";

enum HealthBracket {
	Fine,
	Bruised,
	Injured,
	Critical,
	SIZE
}

export default class Commentator {
	constructor(public usedDescriptions = new Array<Description["id"]>()) {}

	static fromJson(object: any) {
		Object.setPrototypeOf(object, Commentator.prototype);
		return object as Commentator;
	}

	static startFightCommenetary = DescriptionBuilder.quick(
		"",
		"It all depends on your skill!",
		"It's showtime!",
		"Let's party!",
		"Get ready fighters!",
		"Triumph or die!",
		"Fight locale, chosen!",
		"Fists will fly at this location!",
		"The stage of battle is set!",
		"Destination confirmed!",
		"We got two supreme fighters lined up! This is going to be one hell of a show!",
		"And the battle begins!",
		"Let's get started!",
		"Are you ready?",
		"Let's go!",
		"It's the battle of the century!",
		"The battle has begun.",
		"Who will emerge a champion?"
	);

	/*static neutralCommentary = DescriptionBuilder.quick(
		"",
		"The fighters glare at each other.",
		"Its beginning to heat up.",
		"Its just getting started folks.",
		"Nobody blink!",
		"I can't believe my eyes!",
		"Yeah, I've been waitin' for this!",
		"What's next?",
		"And the battle continues!",
		"Are you ready?",
		"Let's keep it up!",
		"Let's pick up the pace!",
		"What will happen now?",
		"Will the tide of battle turn?",
		"Who will come out on top?"
	);

	static advantageCommentary = DescriptionBuilder.quickFormat((fighter, opponent) => {
		const winner = fighter.name;
		const loser = opponent.name;
		return [
			`Looking good for ${winner}.`,
			`Looking bad for ${loser}.`,
			`${loser} is on the ropes!`,
			`${loser} is looking grim.`,
			`Everything is coming up ${winner}`,
			`Can ${winner} close out this fight in the next round?`,
			`Can ${loser} make a comeback?`,
			`Not the best round for ${loser}`,
			`${winner} with the edge on ${loser}. For now.`,
			"Ain't there somebody who can stop this fighting machine?"
		];
	});*/

	static cheerCommentary = DescriptionBuilder.quickFormat(fighter => [
		`${fighter.name} takes an intimidating stance.`,
		`${fighter.name} yells out in excitement.`,
		`${fighter.name} looks encouraged.`,
		`${fighter.name} holds ${fighter.getPosessiveDeterminer()} head up high.`,
		`${fighter.name} nods curtly.`,
		`${fighter.name} gives you a thumbs up.`,
		`${fighter.name} is pumped.`,
		`${fighter.name} is filled with determination.`
	]);

	static booCommentary = DescriptionBuilder.quickFormat(fighter => [
		`${fighter.name} looks discouraged.`,
		`${fighter.name} stomps his foot.`,
		`${fighter.name} spits on the ground.`,
		`${fighter.name} raises ${fighter.getPosessiveDeterminer()} 
		middle finger towards the crowd.`,
		`${fighter.name} hangs ${fighter.getPosessiveDeterminer()} head.`,
		`Your hate only fuels ${fighter.name}'s determination.`
	]);

	/*static healthComments = [
		DescriptionBuilder.quick(""),
		DescriptionBuilder.quickFormat((fighter, other) => [
			`${fighter.name} holds it together.`
		]),
		DescriptionBuilder.quickFormat((fighter, other) => [
			`${fighter.name} looks pissed.`,
			`${fighter.name} is hurt, but still standing.`,
			`${fighter.name} looks hurt.`,
			`Steel your resolve ${fighter.name}, it's not the end!`
		]),
		DescriptionBuilder.quickFormat((fighter, other) => [
			`${fighter.name} is barely standing.`,
			`It's looking really bad for ${fighter.name} right now.`,
			`Not looking so good for ${fighter.name} right now.`,
			`${fighter.name} could fall over any minute.`,
			`${fighter.name} is going to fight to the bitter end.`,
			`You can't give up ${fighter.name}!`,
			`Hey, come on, stand up ${fighter.name}!`
		])
	];*/

	static commentary = [
		//Low Drama
		[
			//Low Advantage
			DescriptionBuilder.quickFormat((winner, loser) => [
				"It's just getting started folks.",
				`The fighters are just getting warmed up.`,
				`${loser.name} looks mad. This should be an exciting match.`,
				"Who will come out on top?"
			]),
			//Mid Advantage
			DescriptionBuilder.quickFormat((winner, loser) => [
				`${loser.name} is furious.`,
				`${loser.name} is holding it together.`,
				`${winner.name} with the early lead, but the fight is far from over.`,
				`If ${winner.name} can keep up this pace, 
				this should be an easy win for ${winner.getObjectPronoun()}.`,
				`${winner.name} has the lead, can ${winner.getPersonalPronoun()} keep it?`
			]),
			//High Advantage
			DescriptionBuilder.quickFormat((winner, loser) => [
				`This is pathetic, ${loser.name}!` //Currently this one is impossible
			])
		],
		//Medium Drama
		[
			//Low Advantage
			DescriptionBuilder.quickFormat((winner, loser) => [
				"What will happen now?",
				`This is anyone's game.`,
				`The fighters size each other up. It could go either way.`,
				`It's looking close, but my money is on ${winner.name}.`,
				`${winner.name} has the advantage. Just barely.`,
				`${winner.name} is managing to keep the lead.`,
				`${winner.name} struggling to hold on to the upper hand.`,
				`I'm sure ${loser.name} has something up his sleeve.`,
				`${loser.name} is biding his time.`,
				`${loser.name} looking for an opening.`,
				`${loser.name} is getting ready to make his move.`
			]),
			//Mid Advantage
			DescriptionBuilder.quickFormat((winner, loser) => [
				`Can ${winner.name} capitalize on this advantage?`,
				`${winner.name} has the advantage.`,
				`${winner.name} is decisively in the lead.`,
				`${winner.name} in the lead. But who knows what could happen nexts.`,

				`${
					loser.name
				} is going to need to up ${loser.getPosessiveDeterminer()} game if ${loser.getPersonalPronoun()} wants to turn this around!`,

				`${
					loser.name
				} is hurting. Can ${loser.getPersonalPronoun()} make a comeback?`,

				`${loser.name} doesn't look so good. Don't give up!`
			]),
			//High Advantage
			DescriptionBuilder.quickFormat((winner, loser) => [
				`${winner.name} with a commanding lead. Can ${loser.name} still ` +
					`turn this around?`,
				`Come on ${loser.name}, get it together!`,
				`Everything is coming up ${winner.name}`
			])
		],
		//High Drama
		[
			//Low Advantage
			DescriptionBuilder.quickFormat((winner, loser) => [
				`Both fighters look unsteady. It's right down to the wire!`,
				`Both fighters barely hanging in there.`,
				`What an incredable fight! Both fighters barely standing. It's anyone's game!`,
				`${winner.name} really wants it, what a close game!`,
				`${winner.name} with the edge, but it's anyone's game!`
			]),
			//Mid Advantage
			DescriptionBuilder.quickFormat((winner, loser) => [
				`Can ${winner.name} close out this fight in the next round?`,
				`${loser.name} could fall over any minute.`,
				`It's looking bad for ${loser.name} right now.`,
				`${loser.name} is hungry, can ` +
					`${loser.getPersonalPronoun()} mount a comeback?`,
				`${loser.name} is tough as nails!`,
				`${loser.name} is not giving up!`,
				`Come on ${loser.name}, not like this!`,

				`${winner.name} with the lead, ${
					loser.name
				} just barely holding on. Is it over?`,

				`${loser.name} is on the ropes!`,
				`${loser.name} is looking grim.`
			]),
			//High Advantage
			DescriptionBuilder.quickFormat((winner, loser) => [
				`What a massacre!`,
				`Not a good day to be a ${loser.name} fan.`,
				`${loser.name} needs a miracle right now.`,
				`${winner.name} is absolutely dominating!`,
				`Just give up ${loser.name}, this is hard to watch`
			])
		]
	];

	startFightCommentary() {
		return this.useDescription(Commentator.startFightCommenetary);
	}

	comment(winner: Fighter, loser: Fighter) {
		let winnerHp = winner.getHealth();
		let loserHp = loser.getHealth();
		if (winnerHp <= 0 || loserHp <= 0) {
			return "";
		}

		const weakestHp = loser.getHealth() / Fighter.MaxHealth;
		const drama = Math.min(2, Math.floor(3 * (1 - weakestHp)));

		const healthDifference = winnerHp - loserHp;
		let advantage = healthDifference < 2 ? 0 : healthDifference < 5 ? 1 : 2; //Whyyyyyy

		const options = Commentator.commentary[drama][advantage];
		return this.useDescription(options, winner, loser);
	}

	interactionComment(fighter: Fighter, isCheer: boolean) {
		const options = isCheer ? Commentator.cheerCommentary : Commentator.booCommentary;
		return this.useDescription(options, fighter);
	}

	private useDescription(
		options: Array<Description>,
		fighter?: Fighter,
		opponent?: Fighter
	) {
		const resultArr = options.filter(description =>
			description.checkConditions(fighter || null, opponent || null)
		);
		const unused = resultArr.filter(desc => !this.usedDescriptions.includes(desc.id));
		if (unused.length === 0) {
			//Reset used descriptions this
			const ids = resultArr.map(desc => desc.id);
			this.usedDescriptions = this.usedDescriptions.filter(id => !ids.includes(id));
		}
		const pickedDesc = pickRandom(unused.length > 0 ? unused : resultArr);

		this.usedDescriptions.push(pickedDesc.id);
		return pickedDesc.text(fighter, opponent);
	}
}
