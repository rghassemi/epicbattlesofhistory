import Fighter from "./Fighter";
import Description from "./Description";
import { pickRandom } from "../util/generalUtility";

// Level
// Target
// Effect(gameState)
// List of Conditions
// Hit Description
// Miss Description
// Win Description

type MoveEffect = (self: Fighter, opponent: Fighter) => void;

export default class Move {
	constructor(
		public name: string,
		public level: number,
		public chance: number,
		public effect: MoveEffect,
		public conditions: Function, //TODO: Type this once we know exactly what we want.
		public hit: Array<Description>,
		public miss: Array<Description>,
		public win: Array<Description>
	) {
		this.name = name;
		this.level = level;
		this.chance = chance;
		this.effect = effect;
		this.conditions = conditions;
		this.hit = hit;
		this.miss = miss;
		this.win = win;
	}

	applyEffect(fighter: Fighter, opponent: Fighter) {
		this.effect(fighter, opponent);
	}

	execute(fighter: Fighter, opponent: Fighter) {
		const hit = Math.random();
		const result = hit <= this.chance;
		if (result) {
			this.applyEffect(fighter, opponent);
		}
		if (!opponent.isDead()) {
			return this.useDescription(fighter, opponent, result ? "hit" : "miss");
		} else {
			return this.useDescription(fighter, opponent, "win");
		}
	}

	useDescription(fighter: Fighter, opponent: Fighter, moveResult: string) {
		let resultArr =
			(moveResult === "hit" && this.hit) ||
			(moveResult === "miss" && this.miss) ||
			this.win;

		resultArr = resultArr.filter(description =>
			description.checkConditions(fighter, opponent)
		);
		if (resultArr.length === 0) {
			throw `ResultsArr is 0, no valid descriptions for move ${
				this.name
			}, fighter is ${fighter.name}, opponent is ${opponent.name}`;
		}
		const unused = resultArr.filter(
			desc => !fighter.usedDescriptions.includes(desc.id)
		);
		if (unused.length === 0) {
			//Reset used descriptions for this move
			const ids = resultArr.map(desc => desc.id);
			fighter.usedDescriptions = fighter.usedDescriptions.filter(
				id => !ids.includes(id)
			);
		}
		const pickedDesc = pickRandom(unused.length > 0 ? unused : resultArr);

		fighter.usedDescriptions.push(pickedDesc.id);
		return pickedDesc.text(fighter, opponent);
	}
}
