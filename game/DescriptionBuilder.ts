import Description from "./Description";
import Person from "./Person";
import Traits from "./Traits";
import { pickRandom } from "../util/generalUtility";

export class FighterPlaceholder {
	name: string;
	traits: any;
	constructor(private prefix: string) {
		this.name = `{${prefix}_NAME}`;
		this.traits = {
			pickNotableWork: () => `{${this.prefix}_NOTABLE_WORK}`
		};
	}
	getPersonalPronoun() {
		return `{${this.prefix}_PP}`;
	}
	getObjectPronoun() {
		return `{${this.prefix}_OP}`;
	}
	getPosessiveDeterminer() {
		return `{${this.prefix}_PD}`;
	}
	getPosessivePronoun() {
		return `{${this.prefix}_PosP}`;
	}
}

type DescriptionFormater = (fighter: Person, opponent: Person) => Array<string>;

export default class DescriptionBuilder {
	private static current = new FighterPlaceholder("FIGHTER");
	private static opponent = new FighterPlaceholder("OPPONNENT");

	private conditions: Description["conditions"] = new Array(() => true);
	private simpleText = new Array<string>();
	private formatText = new Array<string>();
	private results = new Array<Description>();

	private constructor() {}

	static begin(...conditions: Description["conditions"]) {
		return new DescriptionBuilder().withConditions(...conditions);
	}

	static quick(...text: Array<string>) {
		const quickBuilder = new DescriptionBuilder();
		return quickBuilder.simple(...text).get();
	}

	static quickFormat(templateBuilder: DescriptionFormater) {
		const quickBuilder = new DescriptionBuilder();
		return quickBuilder.format(templateBuilder).get();
	}

	withConditions(...conditions: Description["conditions"]) {
		if (conditions.length > 0) {
			this.conditions = conditions;
		}
		return this;
	}

	simple(...descText: Array<string>) {
		descText.forEach(text => this.results.push(this.makeSimpleDesc(text)));
		return this;
	}

	format(templateBuilder: DescriptionFormater) {
		const formatText = templateBuilder(
			DescriptionBuilder.current,
			DescriptionBuilder.opponent
		);
		formatText.forEach(text => this.results.push(this.makeFormatDesc(text)));
		return this;
	}

	get() {
		return this.results;
	}

	reset() {
		this.results = [];
		return this;
	}

	private makeSimpleDesc(text: string) {
		return new Description(
			(fighter: Person, opponent: Person) => text,
			this.conditions
		);
	}

	private makeFormatDesc(template: string) {
		const textFn = (fighter: Person, opponent: Person) => {
			let fighterReplaced = replacePlaceholder(
				template,
				DescriptionBuilder.current,
				fighter
			);
			if (opponent) {
				fighterReplaced = replacePlaceholder(
					fighterReplaced,
					DescriptionBuilder.opponent,
					opponent
				);
			}
			return fighterReplaced;
		};
		return new Description(textFn, this.conditions);

		function replacePlaceholder(text: string, placeholder: Person, fighter: Person) {
			return text
				.replace(new RegExp(placeholder.name, "g"), fighter.name)
				.replace(
					new RegExp(placeholder.getPersonalPronoun(), "g"),
					fighter.getPersonalPronoun()
				)
				.replace(
					new RegExp(placeholder.getObjectPronoun(), "g"),
					fighter.getObjectPronoun()
				)
				.replace(
					new RegExp(placeholder.getPosessiveDeterminer(), "g"),
					fighter.getPosessiveDeterminer()
				)
				.replace(
					new RegExp(placeholder.getPosessivePronoun(), "g"),
					fighter.getPosessiveDeterminer()
				)
				.replace(
					new RegExp(placeholder.traits.pickNotableWork(), "g"),
					fighter.traits.pickNotableWork()
				);
		}
	}
}
