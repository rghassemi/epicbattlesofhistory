import { getFigureId, generateSparQLQuery, getSparqlQueryResults } from "../util/sparql";
import Fighter from "./Fighter";
import { FighterCategory } from "./Fighter";
import Traits from "./Traits";

const propertyIds = {
	Occupation: "P106",
	Nationality: "P27",
	Gender: "P21"
};

type QueryResult = {
	superclass: Array<string>;
	dob: Array<string>;
	dead: Array<string>;
	occupations: Array<string>;
	nationalities: Array<string>;
	gender: Array<string>;
	work: Array<string>;
};

export type GenerateFighterResult = {
	fighter: Fighter;
	name: string;
	valid: boolean;
	reason?: string;
};

const parseQueryResults = async (figureId: string): Promise<QueryResult> => {
	const query = await getSparqlQueryResults(figureId);
	const result: QueryResult = {
		superclass: [],
		dob: [],
		dead: [],
		occupations: [],
		nationalities: [],
		gender: [],
		work: []
	};
	query.forEach((entry: any) => {
		if (entry["superclassLabel"]) {
			result.superclass.push(entry.superclassLabel.value);
		} else if (entry["occupationLabel"]) {
			result.occupations.push(entry.occupationLabel.value);
		} else if (entry["nationalityLabel"]) {
			result.nationalities.push(entry.nationalityLabel.value);
		} else if (entry["dobLabel"]) {
			result.dob.push(entry.dobLabel.value);
		} else if (entry["genderLabel"]) {
			result.gender.push(entry.genderLabel.value);
		} else if (entry["deadLabel"]) {
			result.dead.push(entry.deadLabel.value);
		} else if (entry["workLabel"]) {
			result.work.push(entry.workLabel.value);
		}
	});
	return result;
};

export async function generateFighter(
	name: string,
	allowedCategory = [FighterCategory.Person]
): Promise<GenerateFighterResult> {
	const figure = await getFigureId(name);
	if (!figure) {
		var fighter = new Fighter(name, "", FighterCategory.Unknown);
	} else {
		let { id, label, description } = figure;
		description = description || "";
		const query = await parseQueryResults(id);
		const category = categorize(query);

		var fighter = new Fighter(label, description, category);
		const occupations = query.occupations;
		const nationalities = query.nationalities;
		const gender = query.gender;
		fighter.traits.occupation = occupations;
		fighter.traits.nationality = nationalities;
		fighter.traits.gender =
			gender.length > 0 ? (gender[0] as Traits["gender"]) : "unknown";
		fighter.traits.midlifeYear = getMidlifeYear(query);
		fighter.traits.notableWork = query.work;
	}
	if (!allowedCategory.includes(fighter.category)) {
		return {
			fighter: fighter,
			name: name,
			valid: false,
			reason: `Fighter must be a real human. ${
				fighter.category == FighterCategory.FictionalPerson
					? name + " is fictional."
					: ""
			}`
		};
	}
	return { fighter: fighter, name: name, valid: true };
}

function categorize(info: QueryResult) {
	if (!info.superclass.includes("human")) {
		const isCharacter = info.superclass.some(thing => thing.includes("character"));
		return isCharacter ? FighterCategory.FictionalPerson : FighterCategory.NotPerson;
	}
	return FighterCategory.Person;
}

function getMidlifeYear(info: QueryResult): number {
	if (info.dob.length === 0 && info.dead.length === 0) {
		return NaN;
	}
	if (info.dob.length === 0 && info.dead.length > 0) {
		return jankyParseYear(info.dead[0]);
	} else {
		const dodOrNow =
			info.dead.length > 0
				? jankyParseYear(info.dead[0])
				: new Date().getFullYear();
		const dob = jankyParseYear(info.dob[0]);
		return dob + (dodOrNow - dob) / 2;
	}

	//Wikidata ISO strings for BC times don't have enough padding 0s for Date(). All we
	//need is the year at the front of the string, so just grab it this way instead.
	function jankyParseYear(isoDateStr: string) {
		const split = isoDateStr.split("-");
		const year = isoDateStr[0] === "-" ? "-" + split[1] : split[0];
		return parseInt(year);
	}
}
