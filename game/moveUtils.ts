import Fighter from "./Fighter";
import Traits, { OccupationClass, Status } from "./Traits";
import { pickRandom } from "../util/generalUtility";

export function statusInEffect(status: Status) {
	return (fighter: Fighter, opponent: Fighter) => {
		if (Traits.GoodStatus.includes(status)) {
			return fighter.traits.hasStatus(status);
		} else {
			return opponent.traits.hasStatus(status);
		}
	};
}

export function hasStatus(status: Status) {
	return (fighter: Fighter, opponent: Fighter) => {
		return fighter.traits.hasStatus(status);
	};
}

export function livedBetween(early?: number, late?: number) {
	early = early || Number.NEGATIVE_INFINITY;
	late = late || Number.POSITIVE_INFINITY;
	return (fighter: Fighter, opponent: Fighter) => {
		const life = fighter.traits.midlifeYear;
		return life > early && life < late;
	};
}

export function hasClass(jobClass: OccupationClass) {
	return (fighter: Fighter) => {
		return fighter.traits.hasClass(jobClass);
	};
}

export function hasNotableWork(flags = { opponent: false }) {
	return (fighter: Fighter, opponent: Fighter) => {
		const target = flags.opponent ? opponent : fighter;
		return target.traits.notableWork.length > 0;
	};
}
