import { pickRandom } from "../util/generalUtility";

export type Gender =
	| "male"
	| "female"
	| "transgender male"
	| "transgender female"
	| "unknown";

export type OccupationClass =
	| "Politician"
	| "Actor"
	| "Writer"
	| "Philospher"
	| "Artist"
	| "Musician"
	| "Military"
	| "ReligiousFigure"
	| "Criminal"
	| "Scientist";

export type Status = "Praying" | "Inventing" | "Drawing" | "Poison";

export default class Traits {
	static GoodStatus = ["Praying", "Inventing", "Drawing"];
	static BadStatus = ["Poison"];

	midlifeYear = NaN;
	occupation = new Array<string>();
	nationality = new Array<string>();
	gender: Gender = "unknown";
	notableWork = new Array<string>();
	status = new Array<string>();

	debuffs = new Array<string>();
	expansions = new Array<string>();

	static fromJson(object: any) {
		Object.setPrototypeOf(object, Traits.prototype);
		return object as Traits;
	}

	hasStatus(trait: string) {
		return this.status.includes(trait);
	}

	addStatus(status: Status) {
		this.status.push(status);
	}

	pickNotableWork() {
		return pickRandom(this.notableWork);
	}

	hasClass(occupationClass: OccupationClass) {
		switch (occupationClass) {
			case "Politician":
				return this["occupation"].some(job => /politician|statesman/.test(job));
			case "Actor":
				return this["occupation"].some(job =>
					/actor|stage actor|film actor|voice actor|television actor/.test(job)
				);
			case "Artist":
				return this["occupation"].some(job =>
					/painter|sculptor|illustrator/.test(job)
				);
			case "Writer":
				return this["occupation"].some(job => /novelist|author|poet/.test(job));
			case "Philospher":
				return this["occupation"].some(job => /philosopher/.test(job));
			case "Musician":
				return this["occupation"].some(job =>
					/singer|singer-songwriter|musician|composer/.test(job)
				);
			case "Military":
				return this["occupation"].some(job =>
					/military officer|warrior|military/.test(job)
				);
			case "ReligiousFigure":
				return this["occupation"].some(job => {
					return (
						/religious leader|prophet|preacher|rabbi|mullah|bhikkhu|theologian|mystic/.test(
							job
						) ||
						job.includes("priest") ||
						job.includes("cleric")
					);
				});
			case "Criminal":
				return this["occupation"].some(job =>
					/gangster|outlaw|thief|pirate/.test(job)
				);
			case "Scientist":
				return this["occupation"].some(job =>
					/inventor|scientist|science communicator|science writer|biologist|physicist|psychologist|geologist|geneticist|astronomer/.test(
						job
					)
				);
		}
	}
}
