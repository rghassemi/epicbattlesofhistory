export default class Speech {
	protected speech = new Array<string>();

	protected constructor() {}

	static create = () => {
		return new Speech();
	};

	static quick(words: string) {
		return Speech.create().speak(words);
	}

	static configure(creator: () => Speech) {
		this.create = creator;
	}

	speak(words: string) {
		this.speech.push(words + " ");
		return this;
	}

	endSentence() {
		this.speech.push("\n\n");
		return this;
	}

	pause() {
		this.speech.push("\n");
		return this;
	}

	concat(otherSpeech: Speech) {
		this.speech = this.speech.concat(otherSpeech.speech);
		return this;
	}

	getSpeech() {
		let result: string;
		if (this.speech.length > 1) {
			result = this.speech.reduce((a, b) => a.concat(b));
		} else if (this.speech.length === 1) {
			result = this.speech[0];
		} else result = "";
		return this.sanitize(result);
	}

	toString() {
		return this.getSpeech();
	}

	private sanitize(output: string) {
		return output.replace("&", " and ");
	}
}
