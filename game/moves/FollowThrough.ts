import Move from "../Move";
import Fighter from "../Fighter";
import DescriptionBuilder from "../DescriptionBuilder";
import { statusInEffect, hasStatus, livedBetween } from "../moveUtils";
import Traits from "../Traits";

export const FollowThrough = new Move(
	"FollowThrough",
	0,
	1.0,
	(fighter: Fighter, opponent: Fighter) => {
		opponent.health = opponent.health/2;
	},
	(fighter: Fighter, opponent: Fighter) => {
		const opponentHasDebuff = (opponent: Fighter) => {
			return (
				opponent.traits.status.length > 0 &&
				opponent.traits.status.some(status => Traits.BadStatus.includes(status))
			);
		};
		const fighterHasBuff = (fighter: Fighter) => {
			return (
				fighter.traits.status.length > 0 &&
				fighter.traits.status.some(status => Traits.GoodStatus.includes(status))
			);
		};
		const other = opponentHasDebuff(opponent);
		const self = fighterHasBuff(fighter);
		return other || self;
	},
	DescriptionBuilder.begin()
		.withConditions(statusInEffect("Poison"))
		.format((fighter, opponent) => [`${opponent.name} is looking pretty sick.`])
		.withConditions(statusInEffect("Drawing"))
		.format((self, opponent) => [
			`${self.name} has finished ${self.getPersonalPronoun()} drawing. ` +
				`Its a nude portrait of ${opponent.name}. ` +
				`${opponent.name} is beet red, ` +
				`${opponent.getPersonalPronoun()} can't take it!`,

			`${self.name} has finished ${self.getPersonalPronoun()} drawing. ` +
				`Its a hilarious profile of ${opponent.name}. ` +
				`${opponent.name}! The crowd is laughing! ${opponent.name} can't take it!`
		])
		.withConditions(statusInEffect("Praying"))
		.format((fighter, opponent) => [
			`${opponent.name} is struck by lightning! Unbelievable!`,
			`A part of the ceiling collapses on ${opponent.name}. ` +
				`Looks like ${fighter.name}'s prayers were answered.`
		])
		.withConditions(statusInEffect("Inventing"), livedBetween(1920, null))
		.format((self, opponent) => [
			`${self.name} finishes what ${self.getPersonalPronoun()} was building. ` +
				`It's some kind of laser beam! ${opponent.name} is severely singed.`
		])
		.withConditions(statusInEffect("Inventing"), livedBetween(1700, 1920))
		.format((self, opponent) => [
			`${self.name} finishes what ${self.getPersonalPronoun()} was building. ` +
				`It's some kind of machine gun! ${opponent.name} is hit!`
		])
		.withConditions(statusInEffect("Inventing"), livedBetween(null, 1700))
		.format((self, opponent) => [
			`${self.name} finishes what ${self.getPersonalPronoun()} was building. ` +
				`It's some kind of crossbow! ${opponent.name} is hit!`
		])
		.get(),
	DescriptionBuilder.begin() //Always hits
		//.format((fighter, opponent) => [`${opponent.name} is fighting off the poison!`])
		.get(),
	DescriptionBuilder.begin()
		.withConditions(statusInEffect("Poison"))
		.format((fighter, opponent) => [`${opponent.name} succumbs to the poison!`])
		.withConditions(statusInEffect("Drawing"))
		.format((fighter, opponent) => [
			`${fighter.name} has finished ${fighter.getPersonalPronoun()} drawing.` +
				` Its a nude portrait of ${opponent.name}. ` +
				`${opponent.name} is beet red, ` +
				`${opponent.getPersonalPronoun()} can't take it!`,

			`${fighter.name} has finished ${fighter.getPersonalPronoun()} drawing. ` +
				`Its a hilarious profile of ${opponent.name}. ` +
				`${opponent.name}! The crowd is laughing!` +
				`${opponent.name} can't take it! ${opponent.getPersonalPronoun()}'s ` +
				`storming out of the ring!`
		])
		.withConditions(statusInEffect("Praying"))
		.format((fighter, opponent) => [
			`The earth opens up and swallows ${opponent.name}. ` +
				`Looks like ${fighter.name}'s prayers were answered.`,
			`A bolt of lightning has struck ${opponent.name}! ` +
				`${opponent.getPersonalPronoun()}'s completely toasted.`
		])
		.withConditions(statusInEffect("Inventing"), livedBetween(1920, null))
		.format((self, opponent) => [
			`${self.name} finishes what ${self.getPersonalPronoun()} was building. ` +
				`It's some kind of laser beam! ${opponent.name} is on fire!`
		])
		.withConditions(statusInEffect("Inventing"), livedBetween(1700, 1920))
		.format((self, opponent) => [
			`${self.name} finishes what ${self.getPersonalPronoun()} was building. ` +
				`It's some kind of machine gun! ${opponent.name} is riddled with bullets!`
		])
		.withConditions(statusInEffect("Inventing"), livedBetween(null, 1700))
		.format((self, opponent) => [
			`${self.name} finishes what ${self.getPersonalPronoun()} was building. ` +
				`It's some kind of crossbow! ${self.getPersonalPronoun()} ` +
				`shoots ${opponent.name} in the face!`
		])
		.get()
);
