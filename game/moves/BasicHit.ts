import Move from "../Move";
import Fighter from "../Fighter";
import DescriptionBuilder from "../DescriptionBuilder";
import { hasNotableWork, hasStatus } from "../moveUtils";

export const BasicHit = new Move(
	"BasicHit",
	0,
	0.9,
	(fighter: Fighter, opponent: Fighter) => {
		opponent.health = opponent.health - 1;
	},
	() => true,
	//Hit
	DescriptionBuilder.begin()
		.format((fighter: Fighter, opponent: Fighter) => [
			`${fighter.name} pokes ${opponent.name} in the eye!`,
			`${fighter.name} lands a jab! Two jabs!`,
			`${fighter.name} sweeps the leg!`,
			`${fighter.name} steps on ${opponent.name}'s toes!`,
			`${fighter.name} slaps ${opponent.name} in the face!`,
			`${fighter.name} throws a right! and then a left!`,
			`${fighter.name} kicks ${opponent.name}'s shin!`,
			`${opponent.name} catches a fist with his face!`
		])
		.withConditions(hasNotableWork({ opponent: true }))
		.format((fighter: Fighter, opponent: Fighter) => [
			`${fighter.name} tells ${
				opponent.name
			} that ${opponent.traits.pickNotableWork()} sucks.`,
			`${fighter.name} posts a bad review of ` +
				`${opponent.traits.pickNotableWork()} on the internet.`,
			`${fighter.name} vandalizes a copy of ` +
				`${opponent.traits.pickNotableWork()}!`,
			`${fighter.name} pees on a copy of ` +
				`${opponent.traits.pickNotableWork()}!`
		])
		.withConditions(hasNotableWork())
		.format((fighter, opponent) => [
			`${fighter.name} hits ${opponent.name} upside the head ` +
				`with a copy of ${fighter.traits.pickNotableWork()}.`
		])
		.get(),
	//Miss
	DescriptionBuilder.begin()
		.format((fighter: Fighter, opponent: Fighter) => [
			`${fighter.name} catches dead air trying to kick ${opponent.name}!`,
			`${opponent.name} manages to block ${fighter.name}'s punches!`,
			`${opponent.name} dodges ${fighter.name}!`,
			`${opponent.name} bobs and weaves out of ${fighter.name}'s reach!`,
			`${fighter.name} just barely misses ${opponent.name} with his fist!`
		])
		.withConditions(hasStatus("Poison"))
		.format((fighter: Fighter, opponent: Fighter) => [
			`${fighter.name} is paralyzed by the poison!`
		])
		.withConditions(hasNotableWork({ opponent: true }))
		.format((fighter: Fighter, opponent: Fighter) => [
			`${fighter.name} tries to tell ${opponent.name} that ` +
				`${opponent.traits.pickNotableWork()} sucks, but ` +
				`${opponent.name} believes in ${opponent.getObjectPronoun()}.`,
			`${fighter.name} throws a copy of ${fighter.traits.pickNotableWork()} ` +
				`at ${opponent.name}, but misses.`
		])
		.get(),
	//Kill
	DescriptionBuilder.begin()
		.format((fighter: Fighter, opponent: Fighter) => [
			`${fighter.name} pokes ${
				opponent.name
			} in the eye! Looks like it's lights out for ${opponent.name}!`,
			`${fighter.name} pins ${opponent.name}! 1, 2, 3! ${fighter.name} takes it!`,
			`${fighter.name} throws a right! A left! ${fighter.name} knocks ${
				opponent.name
			} out!`,
			`${fighter.name}'s slap humiliates ${opponent.name} ` +
				`and ${opponent.getPersonalPronoun()} run home crying!`
		])
		.withConditions(hasNotableWork({ opponent: true }))
		.format((fighter: Fighter, opponent: Fighter) => [
			`${fighter.name} takes out a copy of ` +
				`${opponent.traits.pickNotableWork()}, and tears it shreds. The crowd cheers. ` +
				`${opponent.name} is devastated. ` +
				`${opponent.getPersonalPronoun()} collapses.`
		])
		.get()
);
