import Move from "../Move";
import Fighter from "../Fighter";
import DescriptionBuilder from "../DescriptionBuilder";
import { hasClass } from "../moveUtils";

export const HeavyHit = new Move(
	"HeavyHit",
	0,
	0.8,
	(fighter: Fighter, opponent: Fighter) => {
		opponent.health = opponent.health - 2;
	},
	() => true,
	//Hit
	DescriptionBuilder.begin()
		.format((fighter: Fighter, opponent: Fighter) => [
			`${fighter.name} smashes ${opponent.name}'s face into the ground!`,
			`${fighter.name} throws a haymaker at ${opponent.name}!`,
			`${fighter.name} dropkicks ${opponent.name}!`,
			`${fighter.name} lands a huge roundhouse kick on ${opponent.name}!`,
			`${fighter.name} headbutts ${opponent.name}!`,
			`${fighter.name} pummels ${opponent.name} with a flurry of blows!`,
			`${fighter.name} kicks ${opponent.name} in the groin!`
		])
		.withConditions(hasClass("Politician"))
		.format((fighter, opponent) => [
			`${fighter.name}'s staff publishes a scandalous article about ${
				opponent.name
			}! Public oppinion has swayed!`,
			`${fighter.name} heavily implies ${
				opponent.name
			} is having an affair! The crowd believes ${fighter.getObjectPronoun()}!`,
			`${fighter.name} claims ${fighter.getPersonalPronoun()} has proof of ${
				opponent.name
			} comitting a crime! Everyone believes ${fighter.getObjectPronoun()}!`
		])
		.get(),
	//Miss
	DescriptionBuilder.begin()
		.format((fighter: Fighter, opponent: Fighter) => [
			`${opponent.name} breaks out of a poorly executed grapple!`,
			`${fighter.name} punch lands but ${opponent.name} looks unfazed!`,
			`${opponent.name} smirks as ${fighter.name}'s attack does nothing!`,
			`${fighter.name} goes for a suplex but can't pick up ${opponent.name}!`,
			`${fighter.name} goes for a roundhouse but trips!`
		])
		.withConditions(hasClass("Politician"))
		.format((fighter, opponent) => [
			`${fighter.name}'s staff tries to run a smear campaign against ` +
				`${opponent.name}, but it is a flop!`,
			`${fighter.name} posted a scathing article about ${opponent.name} ` +
				`that nobody read!`,
			`${fighter.name} asked for reviews of ${opponent.name} but ` +
				`got back mostly positive replies!`
		])
		.get(),
	//Kill
	DescriptionBuilder.begin()
		.format((fighter: Fighter, opponent: Fighter) => [
			`${fighter.name} kicks ${
				opponent.name
			} in the groin! Ouch, not getting up from that one.`,
			`${fighter.name}'s roundhouse sends ${
				opponent.name
			} into a wall! ${opponent.getPersonalPronoun()} are not getting up from that one!`,
			`${fighter.name} smashes ${
				opponent.name
			}'s into the ground! The referee stops the match!`,
			`${opponent.name} is spinning like a top after ${fighter.name}'s haymaker! ${
				opponent.name
			} falls over!`,
			`${fighter.name} dropkicks ${opponent.name} right in the face! ${
				opponent.name
			} is out cold!`
		])
		.withConditions(hasClass("Politician"))
		.format((fighter, opponent) => [
			`${fighter.name} gets some real dirt on ${opponent.name} and ` +
				`${opponent.name} leaves for ${opponent.getPosessiveDeterminer()} ` +
				`home city!`
		])
		.get()
);
