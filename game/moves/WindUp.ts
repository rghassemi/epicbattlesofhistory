import Move from "../Move";
import Fighter, { FighterCategory } from "../Fighter";
import DescriptionBuilder from "../DescriptionBuilder";
import Traits, { Status } from "../Traits";
import { pickRandom } from "../../util/generalUtility";
import { hasClass, statusInEffect } from "../moveUtils";

export const WindUp = new Move(
	"WindUp",
	0,
	0.6,
	(fighter: Fighter, opponent: Fighter) => {
		const possibleStatus: Array<Status> = ["Poison"];
		if (fighter.traits.hasClass("Artist")) {
			possibleStatus.push("Drawing");
		}
		if (fighter.traits.hasClass("Scientist")) {
			possibleStatus.push("Inventing");
		}
		if (fighter.traits.hasClass("ReligiousFigure")) {
			possibleStatus.push("Praying");
		}
		const chosen = pickRandom(possibleStatus);

		if (Traits.GoodStatus.includes(chosen)) {
			fighter.traits.addStatus(chosen);
		} else {
			opponent.traits.addStatus(chosen);
		}
	},
	(fighter: Fighter, opponent: Fighter) => {
		return fighter.traits.status.length === 0 && opponent.traits.status.length === 0;
	},
	DescriptionBuilder.begin()
		.withConditions(statusInEffect("Poison"))
		.format((fighter, opponent) => [
			`${fighter.name} slips poison into ${opponent.name}'s drink!`,
			`${fighter.name} sprays poison into ${opponent.name}'s face!`
		])
		.withConditions(statusInEffect("Inventing"))
		.format((fighter, opponent) => [
			`${fighter.name} looks like he's building something. What could it be?`,
			`${fighter.name} is building some type of device.`
		])
		.withConditions(statusInEffect("Praying"))
		.format((fighter, opponent) => [
			`${fighter.name} is praying!`,
			`${fighter.name} is praying for a miracle.`
		])
		.withConditions(statusInEffect("Drawing"))
		.format((fighter, opponent) => [
			`${fighter.name} looks like he's drawing something.`,
			`${fighter.name} is drawing something. I wonder what it is.`
		])
		.get(),
	DescriptionBuilder.begin()
		.format((fighter, opponent) => [
			`${fighter.name} slips poison into ${
				opponent.name
			}'s drink, but he spills it all over the floor!`,
			`${fighter.name} slips poison into ${
				opponent.name
			}'s drink, but he doesn't drink it!`
		])
		.simple("") //TODO: Make miss text for all the class specific wind ups
		.get(),
	[] //Can't Win
);
