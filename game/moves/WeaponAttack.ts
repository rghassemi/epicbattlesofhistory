import Move from "../Move";
import Fighter from "../Fighter";
import Description from "../Description";
import DescriptionBuilder from "../DescriptionBuilder";

export const WeaponAttack = new Move(
	"WeaponAttack",
	0,
	0.8,
	(fighter: Fighter, opponent: Fighter) => {
		if (true) {
			opponent.health = opponent.health - 3;
		}
	},
	() => true,
	DescriptionBuilder.begin()
		.format((fighter, opponent) => [
			`${opponent.name} caught the end of ${fighter.name}'s spear!`,
			`${fighter.name} stabs ${opponent.name} with a knife!`,
			`${fighter.name} flogs ${opponent.name} with a cane!`,
			`${fighter.name} cracks ${opponent.name}'s skull open with a hammer!`,
			`${fighter.name} throws a boomerang that smacks ${opponent.name} in the face!`,
			`${opponent.name} takes an arrow to the knee!`,
			`${fighter.name} draws blood with ${fighter.getPosessiveDeterminer()} sword!`,
			`${fighter.name} beats ${opponent.name} with a pipe!`
		])
		.get(),
	DescriptionBuilder.begin()
		.format((fighter, opponent) => [
			`${fighter.name} tried to stab ${
				opponent.name
			} with a butter knife! It wasn't very effective.`,
			`${opponent.name} catches ${fighter.name}'s sword swing with his bare hands!`,
			`${opponent.name} rolls out of the way of ${fighter.name}'s spear thrust!`,
			`${fighter.name} is attacking with a weapon. It didn't do anything. ` +
				`Why did ${fighter.getPersonalPronoun()} think a foam sword would do anything?`,
			`${
				fighter.name
			} creates cracks in the ground with ${fighter.getPosessiveDeterminer()} hammer! ${
				opponent.name
			} gets out of the way just in time!`
		])
		.get(),
	DescriptionBuilder.begin()
		.format((fighter, opponent) => [
			`${fighter.name} takes out a sword and decapitates ${opponent.name}!`,
			`${fighter.name} hits ${opponent.name} with a steel chair! ${
				opponent.name
			} is out cold!`,
			`${fighter.name} bodyslams ${opponent.name} through a table! ${
				opponent.name
			} is down for the count!`
		])
		.get()
);
