import Fighter from "./Fighter";
import { MapObject, pickRandom } from "../util/generalUtility";
import Commentator from "./Commentator";
import Speech from "./Speech";

export enum FightState {
	Matchmaking,
	Betting,
	InRound,
	BetweenRound,
	Interaction,
	End
}

const roundOrders = [[0, 0, 1, 1], [0, 1, 0, 1], [1, 0, 0, 1], [1, 1, 0, 0]];

export default class Fight {
	currentFighter: Fighter = undefined;
	opponent: Fighter = undefined;
	maxRounds: number = 12;
	round: number = 0;
	roundIndex: number = 0;
	currentRoundOrder: Array<number> = [0, 1, 0, 1];
	state: FightState = FightState.Matchmaking;
	interactingFighterIndex: number = -1;
	commentator = new Commentator();

	readonly bets: MapObject<number> = {};

	constructor(public fighters: Array<Fighter>) {
		this.fighters = fighters;
	}

	static fromJson(object: any) {
		if (object.currentFighter)
			object.currentFighter = Fighter.fromJson(object.currentFighter);
		object.fighters = object.fighters.map((obj: any) => Fighter.fromJson(obj));
		object.commentator = Commentator.fromJson(object.commentator);
		Object.setPrototypeOf(object, Fight.prototype);
		return object as Fight;
	}

	static instructions(current: Fight | FightState) {
		const state = current instanceof Fight ? current.state : current;
		const fight = current instanceof Fight ? current : null;
		switch (state) {
			case FightState.Matchmaking:
				return "Choose a fighter. You can name any historical figure. For example, say, I choose George Washington.";
			case FightState.Betting:
				return "Bet money on one of the fighters, or say, No Bets.";
			case FightState.BetweenRound:
			case FightState.InRound:
				return (
					`Fight is in progress between ${fight.fighters[0].name}, ` +
					`and ${fight.fighters[1].name}`
				);
			case FightState.Interaction:
				if (fight) {
					const fighter = fight.fighters[fight.interactingFighterIndex];
					return `Do you cheer or boo, for ${fighter.name}?`;
				} else {
					return "Say cheer or boo.";
				}
			case FightState.End:
				return "Fight has ended. Thanks for playing Wiki Battles.";
		}
	}

	addFighter(fighter: Fighter) {
		this.fighters.push(fighter);
		return Speech.quick(
			`${fighter.name},  ${fighter.description}, has entered the ring!`
		).pause();
	}

	addBet(fighter: Fighter, bet: number) {
		if (!this.bets[fighter.name]) {
			this.bets[fighter.name] = 0;
		}
		this.bets[fighter.name] += bet;
		return `Got it, ${bet} fake dollars on ${fighter.name}.`;
	}

	cheerOrBoo(isCheer: boolean) {
		if (this.interactingFighterIndex === -1) return "";
		const fighter = this.fighters[this.interactingFighterIndex];
		return this.commentator.interactionComment(fighter, isCheer);
	}

	startRound() {
		this.round++;
		this.currentRoundOrder = pickRandom(roundOrders);
		this.roundIndex = 0;
		this.startTurn();
		this.currentFighter.generateMoves(this.opponent);
		this.opponent.generateMoves(this.currentFighter);
		return `${this.fighters[0].name} has ${this.fighters[0].health} health. ${
			this.fighters[1].name
		} has ${this.fighters[1].health} health.`;
	}

	startTurn() {
		const idx = this.roundIndex;
		const fighterIndex = this.currentRoundOrder[idx];
		this.opponent = this.fighters[fighterIndex === 1 ? 0 : 1];
		this.currentFighter = this.fighters[fighterIndex];
	}

	nextState() {
		const speech = Speech.create();
		switch (this.state) {
			case FightState.Matchmaking:
				if (this.fighters.length === 2) {
					speech.speak("Both fighters are ready! Place your bets now.");
					this.state = FightState.Betting;
				} else {
					speech.speak("Choose another fighter.");
				}
				break;
			case FightState.Betting:
				this.state = FightState.InRound;
				speech.speak(this.commentator.startFightCommentary());
				speech.pause();
				this.startRound();
				break;

			case FightState.InRound:
				if (this.isGameOver()) {
					this.state = FightState.End;
				} else if (this.isRoundOver()) {
					this.startRound();
					this.state = FightState.BetweenRound;
				} else {
					this.startTurn();
					const move = this.currentFighter.getMove();
					speech.speak(move.execute(this.currentFighter, this.opponent));
					speech.pause();
					this.roundIndex++;
					this.state = FightState.InRound;
				}
				break;

			case FightState.BetweenRound:
				speech.concat(this.commentate());
				speech.endSentence();

				if (this.round === Math.floor(this.maxRounds / 3)) {
					this.interactingFighterIndex = pickRandom(
						this.fighters.map((fighter, index) => index)
					);
					const interactor = this.fighters[this.interactingFighterIndex];
					speech.speak(
						`${interactor.name} looks to the crowd for support. Do you cheer, or boo?`
					);
					speech.endSentence();
					this.state = FightState.Interaction;
				} else {
					this.state = FightState.InRound;
				}
				break;

			case FightState.Interaction:
				this.state = FightState.InRound;
				break;

			case FightState.End:
				speech.endSentence();
				speech.speak(this.declareWinner());
				speech.pause();
				speech.speak("And that'll do it for this Epic Battle of History!");
				speech.pause();
				speech.speak("Come back anytime!");
				break;
		}

		return speech;
	}

	isRoundOver() {
		return this.isGameOver() || this.roundIndex >= this.currentRoundOrder.length;
	}

	isGameOver() {
		return this.round > this.maxRounds || this.fighters.some(fighter => fighter.isDead());
	}

	private commentate() {
		if (this.isGameOver()) {
			return Speech.create();
		}

		const fightersCopy = this.fighters.slice();
		const [loser, winner] = fightersCopy.sort((a, b) => a.getHealth() - b.getHealth());

		const speech = Speech.create();
		return speech.speak(this.commentator.comment(winner, loser));

		// this.fighters
		// 	.map(fighter => this.commentator.healthComment(winner, loser))
		// 	.forEach(report => speech.speak(report).pause());

		// if (this.round <= 4) {
		// 	return speech.speak(this.commentator.neutralComment());
		// }

		// return speech.speak(this.commentator.advantageComment(winner, loser));
	}

	private declareWinner() {
		const { winner, loser } = this.currentFighter.isDead()
			? { winner: this.opponent.name, loser: this.currentFighter.name }
			: { winner: this.currentFighter.name, loser: this.opponent.name };
		let result = `${winner} has won!`;

		if (this.bets[winner] || this.bets[loser]) {
			const total = (this.bets[winner] || 0) - (this.bets[loser] || 0);
			result +=
				total >= 0
					? `Looks like you made ${total} dollars!`
					: `Looks like someone is out ${Math.abs(total)} fake dollars!`;
		}

		return result;
	}
}
