import moves from "./Moves";
import Description from "./Description";
import Person from "./Person";
import Traits from "./Traits";

// Name
// HealthTypes
// Traits
// Quotes

type MoveIndex = number;

export enum FighterCategory {
	Unknown,
	Person,
	FictionalPerson,
	NotPerson
}

export default class Fighter implements Person {
	health: number;
	moves: Array<MoveIndex>;
	traits: Traits;
	usedDescriptions = new Array<Description["id"]>();
	constructor(
		public name: string,
		public description: string,
		public category: FighterCategory
	) {
		this.name = name;
		this.health = 10;
		this.traits = new Traits();
	}

	static MaxHealth = 8;

	static fromJson(object: any) {
		// object.traits = object.traits.map((trait: any) => Trait.fromJson(trait));
		Object.setPrototypeOf(object, Fighter.prototype);
		return object as Fighter;
	}

	clone() {
		const clone = new Fighter(this.name, this.description, this.category);
		clone.traits = Traits.fromJson(JSON.parse(JSON.stringify(this.traits)));
		return clone;
	}

	addVerbConjugation() {
		switch (this.traits.gender) {
			case "male":
			case "transgender male":
			case "female":
			case "transgender female":
				return true;
			default:
				return false;
		}
	}

	getPersonalPronoun(): string {
		switch (this.traits.gender) {
			case "male":
			case "transgender male":
				return "he";
			case "female":
			case "transgender female":
				return "she";
			default:
				return "they";
		}
	}

	getObjectPronoun() {
		switch (this.traits.gender) {
			case "male":
			case "transgender male":
				return "him";
			case "female":
			case "transgender female":
				return "her";
			default:
				return "them";
		}
	}

	getPosessiveDeterminer() {
		switch (this.traits.gender) {
			case "male":
			case "transgender male":
				return "his";
			case "female":
			case "transgender female":
				return "her";
			default:
				return "their";
		}
	}

	getPosessivePronoun() {
		switch (this.traits.gender) {
			case "male":
			case "transgender male":
				return "his";
			case "female":
			case "transgender female":
				return "hers";
			default:
				return "theirs";
		}
	}

	generateMoves(opponent: Fighter) {
		this.moves = moves
			.map((move, index) => index)
			.filter(moveIndex => moves[moveIndex].conditions(this, opponent));
	}

	getMove() {
		const index = this.moves[Math.floor(Math.random() * this.moves.length)];
		return moves[index];
	}

	//TODO: Just make a single number?
	getHealth() {
		return this.health;
	}

	isDead() {
		return this.health <= 0;
	}
}
