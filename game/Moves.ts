import { BasicHit } from "./moves/BasicHit";
import { HeavyHit } from "./moves/HeavyHit";
import { WeaponAttack } from "./moves/WeaponAttack";
import { WindUp } from "./moves/WindUp";
import { FollowThrough } from "./moves/FollowThrough";

export default [BasicHit, HeavyHit, WeaponAttack, WindUp, FollowThrough];
