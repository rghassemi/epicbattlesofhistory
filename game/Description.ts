import Fighter from "./Fighter";
import Person from "./Person";

// Text
// Optional Condition

type DescriptionGetter = (fighter: Person, opponent: Person) => string;
type Condition = (fighter: Fighter, opponent: Fighter) => boolean;

export default class Description {
	static count = 0;
	id: number;

	constructor(
		public text: DescriptionGetter,
		private conditions: Array<Condition> = [() => true]
	) {
		this.text = text;
		this.conditions = conditions;
		this.id = Description.count++;
	}

	checkConditions(fighter: Fighter, opponent: Fighter) {
		return (
			this.conditions.length > 0 &&
			this.conditions
				.map(fn => fn.call(this, fighter, opponent))
				.reduce((a, b) => a && b)
		);
	}
}
