export default interface Person {
	name: string;
	traits: any;

	getPersonalPronoun(): string;

	getObjectPronoun(): string;

	getPosessiveDeterminer(): string;

	getPosessivePronoun(): string;
}
